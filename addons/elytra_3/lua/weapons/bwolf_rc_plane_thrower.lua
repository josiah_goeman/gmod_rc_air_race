--Copyright 2017 Josiah Goeman
SWEP.PrintName = "RC Plane"
SWEP.Author = "Binary Wolf"
SWEP.Instructions = "Left click to throw an RC plane.  Hold for stronger throw."
SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.DrawCrosshair = true
SWEP.DrawAmmo = false
SWEP.ViewModel = "models/weapons/c_grenade.mdl"
SWEP.WorldModel = "models/weapons/w_grenade.mdl"
SWEP.UseHands = true
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

--script vars
local planeModel = nil
local granadeBoneIndex = nil
local handBoneIndex = nil
local forearmBoneIndex = nil
local armBoneIndex = nil

--new SWEP vars
SWEP.throwStage = 0	--0 = idle, 1 = charging, 2 = throw!
SWEP.throwChargeTimer = 0
SWEP.planeOut = false
SWEP.armAngle = 0
if SERVER then
	SWEP.plane = nil
end
if CLIENT and not IsValid(planeModel) then
	SWEP.addedViewHook = false
	planeModel = ClientsideModel("models/bwolf_rc_plane/bwolf_rc_plane_w.mdl")
		planeModel:SetNoDraw(true)
end

--constants
local maxArmAngle = 100
local minThrowPower = 5
local maxThrowPower = 20
local throwChargePeriod = 1

function SWEP:PrimaryAttack()
end

function SWEP:SecondaryAttack()
end

function SWEP:Deploy()
	if SERVER then
		local owner = self:GetOwner()
		if not IsValid(owner) then return end

		self.plane = ents.Create("bwolf_rc_plane")
		--if not IsValid(self.plane) then return end

		self.plane:SetCreator(owner)
		self.plane:SetPos(owner:EyePos())
		self.plane:SetAngles(owner:EyeAngles())
		self.plane:Spawn()
	end
end

local function Cleanup()
	hook.Remove("PostDrawViewModel", "bwolf_rc_plane_viewmodel")

	--undo the mistakes I've made
	local viewModel = LocalPlayer():GetViewModel()
	if not IsValid(viewModel) then return end

	if grenadeBoneIndex == nil then grenadeBoneIndex = viewModel:LookupBone("ValveBiped.Grenade_Body") end
	if handBoneIndex == nil then handBoneIndex = viewModel:LookupBone("ValveBiped.Bip01_R_Hand") end
	viewModel:ManipulateBonePosition(grenadeBoneIndex, Vector(0, 0, 0))
	viewModel:ManipulateBoneAngles(handBoneIndex, Angle(0, 0, 0))
end


--this is a lazy hack to fake a custom viewmodel, since the plane can be held sort of like a grenade
--¯\_(ツ)_/¯
local posOffset = Vector(3, -2, -12)
local angOffset = Angle(270, 0, 90)
local farFarAway = Vector(-1000, 0, 0)
local handRotation = Angle(0, 0, 60)
local function CustomDrawViewModel()
	--try really hard to find an excuse to avoid work
	local player = LocalPlayer()
	if not IsValid(player) then
		Cleanup()
		return
	end
	local weapon = player:GetActiveWeapon()
	if not IsValid(weapon) then
		Cleanup()
		return
	end
	if weapon:GetClass() ~= "bwolf_rc_plane_thrower" then
		local thrower = player:GetWeapon("bwolf_rc_plane_thrower")
		if IsValid(thrower) then thrower.addedViewHook = false end
		Cleanup()
		return
	end
	local viewModel = player:GetViewModel()
	if not IsValid(viewModel) then
		return
	end

	if grenadeBoneIndex == nil then grenadeBoneIndex = viewModel:LookupBone("ValveBiped.Grenade_Body") end
	if handBoneIndex == nil then handBoneIndex = viewModel:LookupBone("ValveBiped.Bip01_R_Hand") end
	if armBoneIndex == nil then armBoneIndex = viewModel:LookupBone("ValveBiped.Bip01_R_Forearm") end

	--get rid of grenade model by just putting it way behind the camera.  hey, out of sight out of mind right?
	viewModel:ManipulateBonePosition(grenadeBoneIndex, farFarAway)

	--rotate hand to hold plane
	viewModel:ManipulateBoneAngles(handBoneIndex, handRotation)

	--print(armAngle)
	viewModel:ManipulateBoneAngles(armBoneIndex, Angle(weapon.armAngle, 0, 0))

	--draw plane in player's hand
	local boneMatrix = viewModel:GetBoneMatrix(handBoneIndex)
	local planePos, planeAng = LocalToWorld(posOffset, angOffset, boneMatrix:GetTranslation(), boneMatrix:GetAngles())
	planeModel:SetPos(planePos)
	planeModel:SetAngles(planeAng)
	planeModel:DrawModel()

	--and just like that I don't have to piss around with studiomdl for another three hours!
end

function SWEP:DrawHUD()
	if self.planeOut then return end

	local barHeight = 0.1 * ScrH()
	local leftMargin = 0.2 * ScrW()
	local rightMargin = 0.1 * ScrW()
	local bottomMargin = 0.1 * ScrH()
	local innerMargin = 10

	local min = math.min

	draw.RoundedBox(10,
		leftMargin,
		ScrH() - barHeight - bottomMargin,
		ScrW() - leftMargin - rightMargin,
		barHeight,
		Color(255, 255, 255, 100))

	local throwPercent = min(1, self.throwChargeTimer / throwChargePeriod)

	draw.RoundedBox(10,
		leftMargin+innerMargin,
		ScrH() - barHeight - bottomMargin+innerMargin,
		(ScrW() - leftMargin - rightMargin-innerMargin*2) * min(1, throwPercent),
		barHeight-innerMargin*2,
		Color(0, 93, 179, 255))

	--draw.SimpleText(math.floor(throwPercent * 100) .. "%", "CloseCaption_Bold", ScrW()/2, ScrH() - bottomMargin - barHeight / 2, Color(255, 0, 0, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
end

local timer = 0
function SWEP:Think()
	if CLIENT and not self.addedViewHook then
		hook.Add("PostDrawViewModel", "bwolf_rc_plane_viewmodel", CustomDrawViewModel)
		self.addedViewHook = true
	end

	local owner = self:GetOwner()
	if not IsValid(owner) then return end

	if planeOut then return end

	--throw the plane the tick after it was spawned
	--[[if self.readyToDrive and IsValid(self.plane) then
		timer = timer + FrameTime()
		if timer < 1 then return end
		local throwPower = Lerp(self.throwChargeTimer / throwChargePeriod, minThrowPower, maxThrowPower)
		self.plane:SetVelocity(owner:EyeAngles():Forward() * throwPower)
		drive.PlayerStartDriving(self:GetOwner(), self.plane, "bwolf_rc_plane_drive_mode")

		self.throwStage = 0
		self.readyToDrive = false
		self.throwChargeTimer = 0
		timer = 0
		return
	end]]

	if self.throwStage == 0 and owner:KeyDown(IN_ATTACK) then
		self.throwStage = 1
	end
	if self.throwStage == 1 then
		if self.armAngle < maxArmAngle then
			self.armAngle = self.armAngle + FrameTime() * 100
		end

		self.throwChargeTimer = self.throwChargeTimer + FrameTime()

		if not owner:KeyDown(IN_ATTACK) then
			self.armAngle = 0
			self.planeOut = true

			if SERVER then
				--spawn the plane, but don't start driving it yet
				--it won't be ready until the next tick, and if we start now
				--it won't behave properly.  the registered CalcView won't be called, player won't be using the drive animation, and maybe other issues too
				--i don't know why this is.
				local owner = self:GetOwner()
				if not IsValid(owner) then return end
				self.plane:SetPos(owner:EyePos())
				self.plane:SetAngles(owner:EyeAngles())
				
				local throwPower = Lerp(self.throwChargeTimer / throwChargePeriod, minThrowPower, maxThrowPower)
				self.plane:SetVelocity(owner:EyeAngles():Forward() * throwPower)
				drive.PlayerStartDriving(self:GetOwner(), self.plane, "bwolf_rc_plane_drive_mode")
				self.plane:Launch()

				self.throwStage = 0
				self.readyToDrive = false
				self.throwChargeTimer = 0
			end
		end
	end
end

function SWEP:Reset()
	if SERVER then
		self:CallOnClient("Reset")
	end

	self.throwStage = 0	--0 = idle, 1 = charging, 2 = throw!
	self.throwChargeTimer = 0
	self.planeOut = false
	self.armAngle = 0
end
