--Copyright 2017 Josiah Goeman
SWEP.PrintName = "Elytra 3 SWEP"
SWEP.Author = "Binary Wolf"
SWEP.Instructions = "Left click to use a firework; Press space while falling to fly like in Minecraft!"
SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.DrawCrosshair = true
SWEP.DrawAmmo = false
SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.WorldModel = "models/weapons/W_pistol.mdl"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

print("Elytra SWEP v1.0")

--constants
local max = math.max
local min = math.min
local sin = math.sin
local cos = math.cos
local sqrt = math.sqrt
local clamp = math.Clamp
local fireworkLaunch = Sound("elytra/firework_launch.wav")
local hitSounds = {
	Sound("physics/body/body_medium_impact_hard1.wav"),
	Sound("physics/body/body_medium_impact_hard2.wav"),
	Sound("physics/body/body_medium_impact_hard3.wav"),
	Sound("physics/body/body_medium_impact_hard4.wav"),
	Sound("physics/body/body_medium_impact_hard5.wav"),
	Sound("physics/body/body_medium_impact_hard6.wav")
}
local launchBoost = 20
local boostPower = 10
local brakePower = 25
local maxGlideFallSpeed = 3000
local minGlideFallSpeed = 100
local minCrashDamageSpeed = 500
local fovIncreaseSpeed = 3000
local windMinVolumeSpeed = 1500
local windMaxVolumeSpeed = 7000

--SWEP variables
if SERVER then
	SWEP.trail = nil
	SWEP.needsUnstuck = false
end

if CLIENT then
	SWEP.wasFlying = false
	SWEP.windSound = nil
	SWEP.increasedFOV = false
	SWEP.proxyModel = nil
end

local shaking = false

function SWEP:SetupDataTables()
	self:NetworkVar("Bool", 0, "IsFlying");

	if SERVER then
		self:SetIsFlying(false)
	end
end

function SWEP:PrimaryAttack()
	local owner = self:GetOwner()
	if !IsValid(owner) then return end

	self:ShootEffects()
	self:EmitSound(fireworkLaunch, 120, math.random(40)+80, 1)

	self:StartFlying()
end

function SWEP:SecondaryAttack()
end

function SWEP:StartFlying()
	if not SERVER then return end

	if self:GetIsFlying() then return end
	self:SetIsFlying(true)

	local owner = self:GetOwner()
	if(!IsValid(owner)) then return end

	local velocity = owner:GetVelocity()
	--scale velocity down to frametime for the drive system and add launch boost
	--remember! SetVelocity actualls ADDs the vector when used on players for some reason, so we have to subtract our current velocity first
	owner:SetVelocity(-velocity + velocity * FrameTime() + owner:GetForward() * launchBoost)

	--move player up by default view offset and make view offset zero
	owner:SetPos(owner:GetPos() + Vector(0, 0, 64))
	owner:SetCurrentViewOffset(Vector(0, 0, 0))

	owner:SetMoveType(MOVETYPE_NOCLIP)
	drive.PlayerStartDriving(owner, owner, "elytra_drive_mode")

	owner:DoAnimationEvent(PLAYERANIMEVENT_SWIM)

	--trail
	if not IsValid(self.trail) then
		self.trail = util.SpriteTrail(owner, 0, Color(100, 100, 100), false, 50, 500, 5, 1/(50)*0.5, "trails/smoke.vmt")
	end
end

function SWEP:EndFlying(endCamAngles, endVelocity)
	if not SERVER then return end

	if not self:GetIsFlying() then return end
	self:SetIsFlying(false)

	local owner = self:GetOwner()
	if(!IsValid(owner)) then return end

	drive.PlayerStopDriving(owner)
	owner:SetMoveType(MOVETYPE_WALK)

	--PlayerStopDriving sets these back to what they were when PlayerStartDriving was called
	--so we need to manually set them again
	owner:SetEyeAngles(endCamAngles)
	owner:SetVelocity(endVelocity)

	--fix player's camera roll
	local eyeAng = owner:EyeAngles()
	eyeAng.roll = 0
	owner:SetEyeAngles(eyeAng)

	--since collision box is smaller when flying, player will be partially embedded in wall if they crashed
	--this can't be solved here because we're still technically driving (SetPos() won't work on player right now), so save it for the next think iteration
	self.needsUnstuck = true

	if IsValid(self.trail) then
		self.trail:Remove()
	end
end

local modelRotOffset = Angle(90, 0, 0)
local headOffset = Angle(45, 0, 0)
local function DrawProxy(player)
	--print(IsValid(self.proxyModel))
	if not IsValid(player) then return end
	local weapon = player:GetActiveWeapon()
	if not IsValid(weapon) then return end
	local proxyModel = weapon.proxyModel
	if not IsValid(proxyModel) then return end

	local headBoneIndex = proxyModel:LookupBone("ValveBiped.Bip01_Head1")
	if not headBoneIndex then return end

	proxyModel:SetPos(player:GetPos() - player:GetVelocity():Angle():Forward()*32)
	proxyModel:SetAngles(player:GetVelocity():Angle() + modelRotOffset)
	proxyModel:SetupBones()

	local matrix = proxyModel:GetBoneMatrix(headBoneIndex)
	matrix:SetAngles(matrix:GetAngles() - headOffset)
	proxyModel:SetBoneMatrix(headBoneIndex, matrix)

	proxyModel:DrawModel()

	return true	--don't draw default model
end

function SWEP:Think()
	if CLIENT then
		local owner = self:GetOwner()
		if !IsValid(owner) then return end

		local isFlying = self:GetIsFlying()

		--just started flying
		if isFlying and not self.wasFlying then
			--wind sound
			if not IsValid(self.windSound) then self.windSound = CreateSound(self, "elytra/whoosh_fast.wav") end
			self.windSound:ChangeVolume(0, 0)
			self.windSound:Play()

			--draw proxy
			if not IsValid(self.proxyModel) then self.proxyModel = ClientsideModel(owner:GetModel()) end
			hook.Add("PrePlayerDraw", "elytra_proxy_draw", DrawProxy)

			--test
			--print("here")
			--local sequence = owner:LookupSequence( "idle" )
			--print(sequence)
			--owner:SetSequence( sequence )
		end

		--just stopped flying
		if self.wasFlying and not isFlying then
			self.windSound:Stop()
			owner:SetFOV(0, 1)
			self.increasedFOV = false

			self.proxyModel:Remove()
			hook.Remove("elytra_proxy_draw")
		end

		if isFlying then
			local trueSpeed = owner:GetVelocity():Length() / FrameTime()

			--wind sound
			if trueSpeed > windMinVolumeSpeed then
				self.windSound:ChangeVolume(min(1, (trueSpeed-windMinVolumeSpeed) / (windMaxVolumeSpeed-windMinVolumeSpeed)), 0)
			else
				self.windSound:ChangeVolume(0, 0)
			end

			--increase FOV at speed
			if trueSpeed > fovIncreaseSpeed then
				if not self.increasedFOV then
					owner:SetFOV(110, 1)
					self.increasedFOV = true
				end
			else
				if self.increasedFOV then
					owner:SetFOV(0, 1)	--0 returns to user's default setting
					self.increasedFOV = false
				end
			end
		end

		self.wasFlying = isFlying
	end

	if SERVER then
		local owner = self:GetOwner()
		if !IsValid(owner) then return end

		--unsticking stuck players
		if not self.needsUnstuck then return end

		--yes, this is exactly what it looks like, Father forgive me... I couldn't figure out a better way...
		while true do
			local min, max = owner:GetCollisionBounds()
			local playerPos = owner:GetPos()
			local tr = util.TraceHull({
				start	= playerPos,
				endpos	= playerPos,
				mins	= min,
				maxs	= max,
				filter	= owner
			})

			if not tr.StartSolid then break end

			owner:SetPos(playerPos + Vector(math.Rand(-10, 10), math.Rand(-10, 10), math.Rand(-10, 10)))
		end

		self.needsUnstuck = false
	end
end

--[[function SWEP:Initialize()
	if not IsValid(owner) then return end
	if CLIENT then self.proxyModel = ClientsideModel(owner:GetModel()) end
	hook.Add("PrePlayerDraw" , "elytra_proxy_draw" , DrawProxy)
end]]

function SWEP:OnRemove()
	if CLIENT then
		if IsValid(self.proxyModel) then self.proxyModel:Remove() end
		if self.windSound then self.windSound:Stop() end
	end

	if SERVER then
		if IsValid(self.trail) then self.trail:Remove()end
	end

	hook.Remove("elytra_proxy_draw")
end

if CLIENT then
	--[[SWEP.proxyModel = nil

	hook.Add("PrePlayerDraw" , "manual_model_draw_example" , function(ply)
		if not IsValid(ply) then return end
		if not IsValid(proxyModel) then
			proxyModel = ClientsideModel(ply:GetModel())
		end
		if not proxyModel:GetModel() == ply:GetModel() then
			proxyModel:Remove()
			proxyModel = CliendsideModel(ply:GetModel())
		end
		--local playerModel = ClientsideModel(ply:GetModel())
		--if not IsValid(playerModel) then return end
		local headBoneIndex = proxyModel:LookupBone("ValveBiped.Bip01_Head1")
		if not headBoneIndex then return end

		--local matrix = ply:GetBoneMatrix( boneid )

		--if not matrix then
		--	return
		--end

		--local newpos, newang = LocalToWorld( offsetvec, offsetang, matrix:GetTranslation(), matrix:GetAngles() )
		proxyModel:SetPos(ply:GetPos() - ply:EyeAngles():Forward()*32)
		proxyModel:SetAngles(ply:EyeAngles() + Angle(90, 0, 0))
		proxyModel:SetupBones()

		local matrix = proxyModel:GetBoneMatrix(headBoneIndex)
		matrix:SetAngles(matrix:GetAngles() + Angle(-45, 0, 0))
		proxyModel:SetBoneMatrix(headBoneIndex, matrix)

		proxyModel:DrawModel()

		return true	--don't draw default model
	end)]]
end
