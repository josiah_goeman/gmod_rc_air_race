local gravityAccel = 10
local landSpeed = 500	--touching the ground while moving slower than this speed will cause us to land
local crashHurtSpeed = 500	--how fast we have to hit something for it to be considered a crash
local boostPower = 10
local brakePower = 10
local steerPower = 10
local bbMin = Vector(-10, -10, -10)	--collision detection uses much smaller collision box for player so they can squeak through tight spots
local bbMax = Vector(10, 10, 10)

local hitSounds = {
	Sound("physics/body/body_medium_impact_hard1.wav"),
	Sound("physics/body/body_medium_impact_hard2.wav"),
	Sound("physics/body/body_medium_impact_hard3.wav"),
	Sound("physics/body/body_medium_impact_hard4.wav"),
	Sound("physics/body/body_medium_impact_hard5.wav"),
	Sound("physics/body/body_medium_impact_hard6.wav")
}

drive.Register( "elytra_drive_mode",
{
	StartMove = function(self, mv, cmd)
		mv:SetOrigin(self.Entity:GetNetworkOrigin())
		mv:SetVelocity(self.Entity:GetAbsVelocity())
	end,

	Move = function(self, mv)
		--
		-- Set up a speed, go faster if shift is held down
		--
		--[[local speed = 0.05 * FrameTime()
		if ( mv:KeyDown( IN_SPEED ) ) then speed = 0.005 * FrameTime() end

		--
		-- Get information from the movedata
		--
		local ang = mv:GetMoveAngles()
		local pos = mv:GetOrigin()
		local vel = mv:GetVelocity()

		--
		-- Add velocities. This can seem complicated. On the first line
		-- we're basically saying get the forward vector, then multiply it
		-- by our forward speed ( which will be > 0 if we're holding W, < 0 if we're
		-- holding S and 0 if we're holding neither ) - and add that to velocity.
		-- We do that for right and up too, which gives us our free movement.
		--
		vel = vel + ang:Forward() * mv:GetForwardSpeed() * speed
		vel = vel + ang:Right() * mv:GetSideSpeed() * speed
		vel = vel + ang:Up() * mv:GetUpSpeed() * speed

		--
		-- We don't want our velocity to get out of hand so we apply
		-- a little bit of air resistance. If no keys are down we apply
		-- more resistance so we slow down more.
		--
		if ( math.abs( mv:GetForwardSpeed() ) + math.abs( mv:GetSideSpeed() ) + math.abs( mv:GetUpSpeed() ) < 0.1 ) then
			vel = vel * 0.90
		else
			vel = vel * 0.99
		end]]

		local player = self.Entity
		local pos = mv:GetOrigin()
		local ang = mv:GetMoveAngles()
		local vel = mv:GetVelocity()

		local frameTime = FrameTime()

		--gravity
		vel.z = vel.z - gravityAccel * frameTime

		--steering
		--find how fast we are moving relative to our right, and push back accordingly so we go straight again
		local steerForce = ang:Right()
		steerForce:Mul(vel:Dot(steerForce) * -1)
		steerForce:Mul(steerPower * frameTime)

		local preSteerSpeed = vel:Length()
		vel:Add(steerForce)
		--this steering method slows us down proportionally to how hard we steer
		--so we have to manually preserve our speed
		vel:Normalize()
		vel:Mul(preSteerSpeed)

		--lift
		--works pretty much the same as steering, but for up and down instead of left and right
		local liftForce = ang:Up()
		liftForce:Mul(vel:Dot(liftForce) * -1)
		liftForce:Mul(steerPower * frameTime)
		vel:Add(liftForce)
		vel:Normalize()
		vel:Mul(preSteerSpeed)

		--boost
		if(mv:KeyDown(IN_ATTACK)) then
			vel:Add(ang:Forward() * boostPower * frameTime)
		end

		--brake
		if(mv:KeyDown(IN_ATTACK2)) then
			vel:Add(-vel * frameTime)
		end

		--air resistance
		--[[local resistance = Vector()
		resistance:Set(flyVelocity)
		resistance.z = 0
		flyVelocity:Add(resistance * -0.001)]]

		--simple collision detection
		local iterationCount = 0
		while true do
			local tr = util.TraceHull({
				start	= pos,
				endpos	= pos + vel,
				mins	= bbMin,
				maxs	= bbMax,
				filter	= player
			})

			if !tr.Hit then
				pos = pos + vel
				break
			end

			--hit something hard enough to hurt
			local crashSpeed = vel:Dot(-tr.HitNormal)
			if IsFirstTimePredicted() and crashSpeed > crashHurtSpeed * frameTime then
				local weapon = player:GetActiveWeapon()
				if IsValid(weapon) and weapon:GetClass() == "elytra_3" then
					weapon:EndFlying(mv:GetAngles(), vel / frameTime)
				end
				if SERVER then
					player:TakeDamage(crashSpeed*5, player, player)
				end
				player:EmitSound(hitSounds[math.random(6)])

				if not player:Alive() then
					print("ded")
					local ragdoll = player:GetRagdollEntity()
					if IsValid(ragdoll) then
						print("ragdoll!")
						print(ragdoll:GetModel())
						for i = 0, ragdoll:GetPhysicsObjectCount() - 1 do
							print(i)
							local phys = ragdoll:GetPhysicsObjectNum(i)
							if IsValid(phys) then
								phys:ApplyForceCenter( Vector( 0, 0, 10000 ) )
							end
						end
					end
				end
			end

			pos = pos + vel * (tr.Fraction)	--move to collision point
			local remainder = vel * (1 - tr.Fraction)	--leftover velocity
			local scaledCrashSpeed = crashSpeed * frameTime
			vel = remainder + tr.HitNormal * -remainder:Dot(tr.HitNormal)	--project velocity onto hit plane

			--this never seems to happen, but limit the loop so the game doesn't freeze if something weird happens
			if iterationCount == 5 then break end
			iterationCount = iterationCount + 1
		end

		mv:SetVelocity(vel)
		mv:SetOrigin(pos)
	end,

	FinishMove = function(self, mv)
		local player = self.Entity

		local pos = mv:GetOrigin()
		local vel = mv:GetVelocity()

		--apply move
		player:SetNetworkOrigin(pos)
		player:SetAbsVelocity(mv:GetVelocity())
		player:SetAngles(mv:GetMoveAngles())

		local physObject = player:GetPhysicsObject()
		if (SERVER && IsValid(physObject)) then
			physObject:EnableMotion(true)
			physObject:SetPos(mv:GetOrigin());
			physObject:Wake()
			physObject:EnableMotion(false)
		end

		--keep player from turning off noclip while flying
		--otherwise drive system has a conniption when we touch anything
		player:SetMoveType(MOVETYPE_NOCLIP)

		local weapon = player:GetActiveWeapon()
		if(not IsValid(weapon) or not weapon:GetClass() == "elytra_3") then return end

		--check if we're in water
		local tr = util.TraceLine({
				start	= pos,
				endpos	= pos,
				filter	= player,
				mask = MASK_WATER
		})

		if tr.Hit then
			weapon:EndFlying(mv:GetAngles(), (mv:GetVelocity() / FrameTime()) - player:GetVelocity())
			return
		end

		--are we moving slowly enough to land?
		local scaledLandSpeed = landSpeed * FrameTime()
		if vel:LengthSqr() < scaledLandSpeed * scaledLandSpeed then
			--check if we're on the ground
			--local min, max = self.Entity:GetCollisionBounds()
			tr = util.TraceHull({
				start	= pos,
				endpos	= pos + Vector(0, 0, -1),
				mins = bbMin,
				maxs = bbMax,
				filter	= player
			})
			if tr.Hit then
				weapon:EndFlying(mv:GetAngles(), (mv:GetVelocity() / FrameTime()) - player:GetVelocity())
				return
			end
		end

		--quit if space is pressed
		if mv:KeyDown(IN_JUMP) then
			weapon:EndFlying(mv:GetAngles(), (mv:GetVelocity() / FrameTime()) - player:GetVelocity())
		end
	end,

	CalcView = function(self, view)
		local player = self.Entity

		local vel = player:GetVelocity()
		local speed = vel:Length()

		--roll camera when turning
		local velDir = player:GetVelocity()
		velDir.z = 0
		velDir:Normalize()
		local velYaw = velDir:Angle().yaw
		local viewAng = player:EyeAngles()
		local viewYaw = viewAng.yaw
		
        --velYaw = velYaw + 1000
        --viewYaw = viewYaw + 1000

        local shortestAngle = viewYaw - velYaw;
        if shortestAngle < -180 then shortestAngle = shortestAngle + 360 end

        if shortestAngle > 90 then shortestAngle = 180 - shortestAngle end
        if shortestAngle < -90 then shortestAngle = -180 - shortestAngle end

        viewAng.roll = -shortestAngle / 2
        player:SetEyeAngles(viewAng)

		--local idealdist = math.max( 10, self.Entity:BoundingRadius() ) * 4
		--self:CalcView_ThirdPerson( view, idealdist, 2, { self.Entity } )
	end,
}, "drive_base");
