--Copyright 2017 Josiah Goeman
AddCSLuaFile("entities/bwolf_rc_plane/shared.lua")
AddCSLuaFile("weapons/bwolf_rc_plane_thrower.lua")
--resource.AddFile("sound/elytra/firework_launch.wav")
resource.AddFile("sound/elytra/whoosh_fast.wav")

print("RC Air Race V1.0")

local gravityAccel = 10
local minBumpSpeed = 200
local minCrashSpeed = 500	--how fast we have to hit something for it to be considered a crash
local boostPower = 10
local brakePower = 10
local steerPower = 10
local minLiftSpeed = 750
local bbMin = Vector(-5, -5, -5)
local bbMax = Vector(5, 5, 5)
local bumpSounds = {
	Sound("physics/plastic/plastic_barrel_impact_soft1.wav"),
	Sound("physics/plastic/plastic_barrel_impact_soft2.wav"),
	Sound("physics/plastic/plastic_barrel_impact_soft3.wav"),
	Sound("physics/plastic/plastic_barrel_impact_soft4.wav"),
	Sound("physics/plastic/plastic_barrel_impact_soft5.wav"),
	Sound("physics/plastic/plastic_barrel_impact_soft6.wav")
}
local crashSounds = {
	Sound("vehicles/v8/vehicle_impact_heavy1.wav"),
	Sound("vehicles/v8/vehicle_impact_heavy2.wav"),
	Sound("vehicles/v8/vehicle_impact_heavy3.wav"),
	Sound("vehicles/v8/vehicle_impact_heavy4.wav")
}

drive.Register("bwolf_rc_plane_drive_mode",
{
	CalcView = function(self, view)
		local plane = self.Entity
		if not IsValid(plane) then return end

		--
		if plane:GetIsDestroyed() then
			self:CalcView_ThirdPerson(view, 200, 2, {self.Entity})
			view.angles.roll = 0
			return
		end
		
		--thirdperson with 0 distance is effectively firstperson
		self:CalcView_ThirdPerson(view, 0, 2, {self.Entity})

		--roll camera when turning
		local velDir = plane:GetVelocity()
		local velYaw = velDir:Angle().yaw

        local shortestAngle = view.angles.yaw - velYaw;

        --range becomes -180-180
        if shortestAngle < -180 then shortestAngle = shortestAngle + 360 end

        --allow us to face backwards without camera going upside down
        if shortestAngle > 90 then shortestAngle = 180 - shortestAngle end
        if shortestAngle < -90 then shortestAngle = -180 - shortestAngle end

        --integrate this value in the self table for now
		self.viewRoll = (self.viewRoll or 0) * 0.9 + (-shortestAngle / 2) * 0.1
		view.angles.roll = self.viewRoll
	end,

	StartMove = function(self, mv, cmd)
		if self.Entity:GetIsDestroyed() then return end

		mv:SetOrigin(self.Entity:GetNetworkOrigin())
		mv:SetVelocity(self.Entity:GetAbsVelocity())
	end,

	Move = function(self, mv)
		local plane = self.Entity
		if plane:GetIsDestroyed() then return end

		local pos = mv:GetOrigin()
		local ang = mv:GetMoveAngles()
		local vel = mv:GetVelocity()

		local frameTime = FrameTime()

		--gravity
		vel.z = vel.z - gravityAccel * frameTime

		--steering
		--find how fast we are moving relative to our right, and push back accordingly so we go straight again
		local steerForce = ang:Right()
		steerForce:Mul(vel:Dot(steerForce) * -1)
		steerForce:Mul(steerPower * frameTime)

		local preSteerSpeed = vel:Length()
		vel:Add(steerForce)
		--this steering method slows us down proportionally to how hard we steer
		--so we have to manually preserve our speed
		vel:Normalize()
		vel:Mul(preSteerSpeed)

		--lift
		--works pretty much the same as steering, but for up and down instead of left and right
		local liftForce = ang:Up()
		liftForce:Mul(vel:Dot(liftForce) * -1)
		liftForce:Mul(steerPower * frameTime)
		if preSteerSpeed < (minLiftSpeed * frameTime) then liftForce:Mul(0) end
		vel:Add(liftForce)
		vel:Normalize()
		vel:Mul(preSteerSpeed)

		--boost
		if(mv:KeyDown(IN_ATTACK)) then
			vel:Add(ang:Forward() * boostPower * frameTime)
		end

		--brake
		if(mv:KeyDown(IN_ATTACK2)) then
			vel:Add(-vel * frameTime)
		end

		--air resistance
		--[[local resistance = Vector()
		resistance:Set(flyVelocity)
		resistance.z = 0
		flyVelocity:Add(resistance * -0.001)]]
		plane.isScraping = false

		--simple collision detection
		local iterationCount = 0
		while true do
			local tr = util.TraceHull({
				start	= pos,
				endpos	= pos + vel,
				mins	= bbMin,
				maxs	= bbMax,
				filter	= {self.Player}	--plane has no physics, so don't worry about it
			})

			if !tr.Hit then
				pos = pos + vel
				break
			end

			plane.isScraping = true

			--hit something hard enough to hurt
			local collisionSpeed = vel:Dot(-tr.HitNormal)
			if IsFirstTimePredicted() then
				if collisionSpeed > minBumpSpeed * frameTime then plane:EmitSound(bumpSounds[math.random(6)]) end
				if collisionSpeed > minCrashSpeed * frameTime then
					--[[local effectdata = EffectData()
					effectdata:SetOrigin(pos)
					util.Effect("Explosion", effectdata)
					plane:EmitSound(crashSounds[math.random(4)])
					plane.isScraping = false]]
					--plane:SetIsDestroyed(true)
				end
			end

			pos = pos + vel * (tr.Fraction)	--move to collision point
			local remainder = vel * (1 - tr.Fraction)	--leftover velocity
			vel = remainder + tr.HitNormal * -remainder:Dot(tr.HitNormal)	--project velocity onto hit plane

			--this never seems to happen, but limit the loop so the game doesn't freeze if something weird happens
			if iterationCount == 5 then break end
			iterationCount = iterationCount + 1
		end

		mv:SetVelocity(vel)
		mv:SetOrigin(pos)
	end,

	FinishMove = function(self, mv)
		if self.Entity:GetIsDestroyed() then return end

		self.Entity:SetNetworkOrigin(mv:GetOrigin())
		self.Entity:SetAbsVelocity(mv:GetVelocity())
		self.Entity:SetAngles(mv:GetMoveAngles())
	end,
}, "drive_base");
