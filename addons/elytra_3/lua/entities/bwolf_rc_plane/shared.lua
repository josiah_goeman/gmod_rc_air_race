--Copyright 2017 Josiah Goeman
ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "RC Plane"
ENT.Author = "Binary Wolf"
ENT.Contact = "Binary Wolf on Steam"
ENT.Editable = false
ENT.Spawnable = false
ENT.AdminOnly = false

--constants
local min = math.min
local floor = math.floor
local catchSound = Sound("physics/body/body_medium_impact_soft7.wav")
local windMinVolumeSpeed = 750
local windMaxVolumeSpeed = 10000
local fovIncreaseSpeed = 1000
local catchDistance = 75
local maxCatchSpeed = 1000
local MAX_PROPELLED_SPEED = 2000
local swooshBBMin = Vector(-20, -20, -20)
local swooshBBMax = Vector(20, 20, 20)
local swooshSpeed = 1000
local swooshSounds = {
	Sound("elytra/swoosh_1.wav"),
	Sound("elytra/swoosh_2.wav")
}
local crashSounds = {
	Sound("vehicles/v8/vehicle_impact_heavy1.wav"),
	Sound("vehicles/v8/vehicle_impact_heavy2.wav"),
	Sound("vehicles/v8/vehicle_impact_heavy3.wav"),
	Sound("vehicles/v8/vehicle_impact_heavy4.wav")
}

local WAITING_FOR_LAUNCH = 1
local FLYING = 2
local DESTROYED = 3

--ENT variables
ENT.state = WAITING_FOR_LAUNCH
if SERVER then
	ENT.canBeCaught = false
	ENT.creatorPos = nil
	ENT.isScraping = false
	ENT.wasScraping = false
	ENT.scrapeSound = nil
	
	ENT.increasedFOV = false
	ENT.leftTrail = nil
	ENT.rightTrail = nil
	ENT.swooshHitPrev = false
	ENT.prevPos = nil
	ENT.prevSpeed = 0
end
if CLIENT then
	ENT.showingDeathScreen = false
	ENT.propellerSound = nil
	ENT.motorSound = nil
	ENT.windSound = nil
end

local function DrawRespawnText(self)
	if self:GetIsDestroyed() and CurTime() % 1.5 < 0.5 then return end
	draw.Text({text = "press JUMP to continue",font = "DermaLarge",  pos = {ScrW()/2, ScrH()-ScrH()/4}, xalign = TEXT_ALIGN_CENTER})
end

local function DrawBinocularOverlay()
	DrawMaterialOverlay("effects/combine_binocoverlay", 0)
end

local function DrawTVOverlay()
	DrawMaterialOverlay("effects/tvscreen_noise002a", 0)
end

function ENT:SetupDataTables()
	self:NetworkVar("Bool", 0, "IsDestroyed")
	self:SetIsDestroyed(false)
end

function ENT:Initialize()
	self:SetModel("models/bwolf_rc_plane/bwolf_rc_plane_w.mdl")
	self:SetMoveType(MOVETYPE_CUSTOM)

	if CLIENT then
		self.propellerSound = CreateSound(self, "vehicles/airboat/fan_blade_idle_loop1.wav")
		self.propellerSound:Play()
		self.propellerSound:ChangeVolume(0.5, 0)
		self.motorSound = CreateSound(self, "vehicles/airboat/fan_blade_fullthrottle_loop1.wav")
		self.motorSound:Play()
		self.motorSound:ChangeVolume(0, 0)
		self.windSound = CreateSound(self, "elytra/whoosh_fast.wav")
		self.windSound:Play()
		self.windSound:ChangeVolume(0, 0)

		hook.Add("HUDPaint", "bwolf_rc_plane_text", function()
			local barHeight = 0.1 * ScrH()
			local leftMargin = 0.2 * ScrW()
			local rightMargin = 0.1 * ScrW()
			local bottomMargin = 0.1 * ScrH()
			local innerMargin = 10

			local speed = self:GetVelocity():Length()

			draw.RoundedBox(10,
				leftMargin,
				ScrH() - barHeight - bottomMargin,
				ScrW() - leftMargin - rightMargin,
				barHeight,
				Color(255, 255, 255, 100))

			local speedPercent = speed / MAX_PROPELLED_SPEED

			draw.RoundedBox(10,
				leftMargin+innerMargin,
				ScrH() - barHeight - bottomMargin+innerMargin,
				(ScrW() - leftMargin - rightMargin-innerMargin*2) * min(1, speedPercent),
				barHeight-innerMargin*2,
				Color(0, 93, 179, 255))

			if self:GetIsDestroyed() then
				if CurTime() % 1.5 < 0.5 then return end
				draw.Text({
					text = "press JUMP to continue",
					font = "DermaLarge",
					pos = {ScrW()/2, ScrH()-ScrH()/4},
					xalign = TEXT_ALIGN_CENTER,
					color = Color(255, 255, 255, 255)
					})
			else
				draw.Text({
					text = floor(speed * 0.01905) .. "m/s",	--0.3048 feet/meter / 16 hammer units/foot = 0.01905 hu/m
					font = "DermaLarge",
					pos = {ScrW()/2, ScrH()-ScrH()/4},
					xalign = TEXT_ALIGN_CENTER,
					color = Color(255, 255, 255, 255)
					})
			end
		end)
		hook.Add("RenderScreenspaceEffects", "bwolf_rc_plane_binocular_overlay", DrawBinocularOverlay)
	end

	if SERVER then
		local creator = self:GetCreator()
		if IsValid(creator) then self.creatorPos = creator:GetPos() + creator:OBBCenter() end
		self.scrapeSound = CreateSound(self, "physics/plastic/plastic_barrel_scrape_smooth_loop1.wav")
		self.scrapeSound:Play()
		self.scrapeSound:ChangeVolume(0, 0)
		

		self.leftTrail = util.SpriteTrail(self, self:LookupAttachment("trail_attach_left"), Color(100, 0, 0), false, 50, 0, 10, 1/(50)*0.5, "trails/laser.vmt")
		self.rightTrail = util.SpriteTrail(self, self:LookupAttachment("trail_attach_right"), Color(100, 0, 0), false, 50, 0, 10, 1/(50)*0.5, "trails/laser.vmt")
	end
end

function ENT:OnRemove()
	if CLIENT then
		self.propellerSound:Stop()
		self.motorSound:Stop()
		self.windSound:Stop()

		hook.Remove("HUDPaint", "bwolf_rc_plane_text")
		hook.Remove("RenderScreenspaceEffects", "bwolf_rc_plane_tv_overlay")
		hook.Remove("RenderScreenspaceEffects", "bwolf_rc_plane_binocular_overlay")
	end

	if SERVER then
		self.scrapeSound:Stop()

		if IsValid(self.leftTrail) then self.leftTrail:Remove() end
		if IsValid(self.rightTrail) then self.rightTrail:Remove() end

		local creator = self:GetCreator()
		if IsValid(creator) then creator:SetFOV(0, 0) end
	end
end

function ENT:Launch()
	if SERVER then
		
	end

	

	self.mode = FLYING
	print("launched")
end

function ENT:Think()
	if CLIENT then
		--motor sound
		if input.IsMouseDown(MOUSE_LEFT) or input.IsMouseDown(MOUSE_RIGHT) then
			self.motorSound:ChangeVolume(1, 0)
			self.propellerSound:ChangeVolume(0, 0.5)
		else
			self.motorSound:ChangeVolume(0, 0.5)
			self.propellerSound:ChangeVolume(0.5, 0)
		end

		--wind sound
		local speed = self:GetVelocity():Length()
		if speed > windMinVolumeSpeed then
			self.windSound:ChangeVolume(min(1, (speed-windMinVolumeSpeed) / (windMaxVolumeSpeed-windMinVolumeSpeed)), 0)
		else
			self.windSound:ChangeVolume(0, 0)
		end

		if not self.showingDeathScreen and self:GetIsDestroyed() then
			--hook.Add("HUDPaint", "bwolf_rc_plane_text", DrawRespawnText)
			hook.Add("RenderScreenspaceEffects", "bwolf_rc_plane_tv_overlay", DrawTVOverlay)
			hook.Remove("RenderScreenspaceEffects", "bwolf_rc_plane_binocular_overlay")
			self.showingDeathScreen = true
		end

		return
	end

	local pos = self:GetPos()
	local speed = self:GetVelocity():Length()
	local creator = self:GetCreator()

	--owner is gone
	if not IsValid(creator) or not creator:Alive() then
		self:Remove()
		return
	end

	if not self:GetIsDestroyed() then
		--check for crashes
		if self:WaterLevel() > 0 or speed - self.prevSpeed < -500 then
			local effectdata = EffectData()
			effectdata:SetOrigin(pos)
			util.Effect("Explosion", effectdata)
			self:EmitSound(crashSounds[math.random(4)])
			self.isScraping = false
			self:SetIsDestroyed(true)
		end
		self.prevSpeed = speed

		

		--scraping sound
		if self.isScraping and not self.wasScraping then
			self.scrapeSound:ChangeVolume(0.5, 0)
		end
		if self.wasScraping and not self.isScraping then
			self.scrapeSound:ChangeVolume(0, 0.25)
		end
		self.wasScraping = self.isScraping

		--swoosh sound
		local tr = util.TraceHull({
			start	= prevPos,
			endpos	= pos,
			mins	= swooshBBMin,
			maxs	= swooshBBMax
		})
		if tr.Hit then
			--play swoosh sfx
			if speed > swooshSpeed and not self:GetIsDestroyed() and not self.swooshHitPrev then
				sound.Play(swooshSounds[math.random(2)], tr.HitPos, 75, 100, 1)
				--sound.Play("ambient/explosions/exp1.wav", tr.HitPos)
			end
		end
		self.swooshHitPrev = tr.Hit
		self.prevPos = pos

		--increase FOV at speed
		if speed > fovIncreaseSpeed then
			if not self.increasedFOV then
				creator:SetFOV(110, 1)
				self.increasedFOV = true
			end
		else
			if self.increasedFOV then
				creator:SetFOV(0, 1)	--0 returns to user's default setting
				self.increasedFOV = false
			end
		end
		
		--owner catches plane
		if speed < maxCatchSpeed and (self.creatorPos - pos):LengthSqr() < catchDistance * catchDistance then
			if self.canBeCaught then
				local weapon = creator:GetActiveWeapon()
				if IsValid(weapon) and weapon:GetClass() == "bwolf_rc_plane_thrower" then weapon:Reset() end

				creator:EmitSound(catchSound)
				drive.PlayerStopDriving(creator)
				self:Remove()
				return
			end
		else
			self.canBeCaught = true
		end
	end

	--plane is destroyed, waiting for player to continue
	if self:GetIsDestroyed() then
		--don't draw the shadow
		self:SetNoDraw(true)

		--restore FOV
		creator:SetFOV(0, 1)	--0 returns to user's default setting
		self.scrapeSound:ChangeVolume(0, 0.25)

		--remove when player presses space
		if creator:KeyDown(IN_JUMP) then
			drive.PlayerStopDriving(creator)
			self:Remove()

			local weapon = creator:GetActiveWeapon()
			if IsValid(weapon) and weapon:GetClass() == "bwolf_rc_plane_thrower" then weapon:Reset() end
		end
	end

	self:NextThink(CurTime())
	return true
end
