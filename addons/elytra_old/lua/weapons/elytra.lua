--Copyright 2017 Josiah Goeman
SWEP.PrintName = "Elytra SWEP"
SWEP.Author = "Binary Wolf"
SWEP.Instructions = "Left click to use a firework; Press space while falling to fly like in Minecraft!"
SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.DrawCrosshair = true
SWEP.DrawAmmo = false
SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.WorldModel = "models/weapons/W_pistol.mdl"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = true
SWEP.Secondary.Ammo	= "none"

print("Elytra SWEP v1.0")

--constants
local max = math.max
local sin = math.sin
local cos = math.cos
local sqrt = math.sqrt
local fireworkLaunch = Sound("elytra/firework_launch.wav")
local rocketImpulse = Vector(0, 0, 1000)
local maxGlideFallAccel = 15
local minGlideFallAccel = 1
local minCrashImpact = 100

local deployed = false
local flyVelocity = Vector(0, 0, 0)

function SWEP:PrimaryAttack()
	local owner = self:GetOwner()
	if !IsValid(owner) then return end

	if(owner:IsOnGround()) then
		self:ShootEffects()
		sound.Play(fireworkLaunch, owner:EyePos(), 120, math.random(40)+80, 1)
		owner:SetVelocity(rocketImpulse)
	else
		
	end
end

function SWEP:SecondaryAttack()
	
end

function setDeployed(swep, value)
	local owner = swep:GetOwner()
	if(!IsValid(owner)) then return end

	deployed = value
	if deployed then
		flyVelocity = owner:GetVelocity()
		owner:SetGravity(0.00001)
	else
		owner:SetGravity(1)
	end
end


function SWEP:Think()
	if SERVER then
		local owner = self:GetOwner()
		if(!IsValid(owner)) then return end

		if(owner:IsOnGround()) then
			setDeployed(self, false)
		else
			if(owner:KeyPressed(IN_JUMP) && owner:GetVelocity().z < 0) then
				setDeployed(self, true)
			end
		end

		if deployed then
			local lookDirection = owner:GetForward()

			if owner:KeyPressed(IN_ATTACK) then
				--flyVelocity = owner:EyeAngles():Forward() * 1000
			end

			--glide
			local pitchRad = owner:GetAngles().pitch * 0.0174533
			local glidePower = cos(pitchRad)
			glidePower = glidePower * glidePower
			flyVelocity.z = flyVelocity.z + -maxGlideFallAccel + glidePower * (maxGlideFallAccel - minGlideFallAccel)
			--owner:SetVelocity(Vector(0, 0, -maxGlideFallAccel + glidePower * (maxGlideFallAccel - minGlideFallAccel)))

			local lookHLength = sqrt(lookDirection.x * lookDirection.x + lookDirection.y * lookDirection.x)

			--push forward while gliding
			if(flyVelocity.z < 0) then
				local forwardAccel = flyVelocity.z * -0.1 * glidePower
				--flyVelocity.z = flyVelocity.z + forwardAccel * 0.1
				flyVelocity.x = flyVelocity.x + lookDirection.x * forwardAccel
				flyVelocity.y = flyVelocity.y + lookDirection.y * forwardAccel
			end

			local hSpeed = sqrt(flyVelocity.x * flyVelocity.x + flyVelocity.y * flyVelocity.y)

			--aiming up
			if pitchRad < 0 then
				local lift = max(hSpeed * 0.1, 0.1) * -sin(pitchRad * 0.5)
				print(lookHLength)
				flyVelocity.z = flyVelocity.z + lift
				--flyVelocity.x = flyVelocity.x - lookDirection.x * lift / lookHLength
				--flyVelocity.y = flyVelocity.y - lookDirection.y * lift / lookHLength
			end

			--drag
			flyVelocity.x = flyVelocity.x - flyVelocity.x * 0.025
			flyVelocity.y = flyVelocity.y - flyVelocity.y * 0.025

			owner:SetVelocity(-owner:GetVelocity() + flyVelocity)
		end
	end
end
