--Copyright 2017 Josiah Goeman
SWEP.PrintName = "Elytra 2 SWEP"
SWEP.Author = "Binary Wolf"
SWEP.Instructions = "Left click to use a firework; Press space while falling to fly like in Minecraft!"
SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.DrawCrosshair = true
SWEP.DrawAmmo = false
SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.WorldModel = "models/weapons/W_pistol.mdl"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

--SWEP.Secondary.ClipSize = -1
--SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = true
--SWEP.Secondary.Ammo	= "none"

print("Elytra SWEP v1.0")

--constants
local max = math.max
local min = math.min
local sin = math.sin
local cos = math.cos
local sqrt = math.sqrt
local clamp = math.Clamp
local fireworkLaunch = Sound("elytra/firework_launch.wav")
local hitSounds = {
	Sound("physics/body/body_medium_impact_hard1.wav"),
	Sound("physics/body/body_medium_impact_hard2.wav"),
	Sound("physics/body/body_medium_impact_hard3.wav"),
	Sound("physics/body/body_medium_impact_hard4.wav"),
	Sound("physics/body/body_medium_impact_hard5.wav"),
	Sound("physics/body/body_medium_impact_hard6.wav")
}
local rocketImpulse = Vector(0, 0, 1000)
local boostPower = 10
local brakePower = 25
local maxGlideFallSpeed = 3000
local minGlideFallSpeed = 100
local minCrashDamageSpeed = 500
local fovIncreaseSpeed = 1500

local deployed = false
local flyVelocity = Vector(0, 0, 0)

local increasedFOV = false
local shaking = false
local justLaunched = false

function SWEP:PrimaryAttack()
	local owner = self:GetOwner()
	if !IsValid(owner) then return end

	--if owner:IsOnGround() or owner:WaterLevel() > 0 then
		self:ShootEffects()
		self:EmitSound(fireworkLaunch, 120, math.random(40)+80, 1)
		
		owner:SetVelocity(owner:GetForward() * 1000)
		flyVelocity = owner:GetForward() * 1000
		setDeployed(self, true)
		justLaunched = true
	--end
end

function SWEP:SecondaryAttack()
	--self:GetOwner():SetPos(self:GetOwner():GetPos() + Vector(0, 0, 100))
end

local trail
function setDeployed(swep, value)
	local owner = swep:GetOwner()
	if(!IsValid(owner)) then return end

	deployed = value
	if deployed then
		flyVelocity = owner:GetVelocity()
		owner:SetGravity(0.00001)	--can't actually set to zero because it has no effect for some reason, but this close enough for this purpose
		if not IsValid(trail) then
			--trail = util.SpriteTrail(owner, 0, Color(100, 0, 0), false, 50, 0, 5, 1/(10+0)*0.5, "cable/xbeam.vmt")
		end
	else
		owner:SetGravity(1)
		owner:SetFOV(0, 1)
		if IsValid(trail) then trail:Remove() end
	end
end

function SWEP:Think()
	if SERVER then
		local owner = self:GetOwner()
		if(!IsValid(owner)) then return end

		--deploy and put away
		if owner:IsOnGround() or owner:WaterLevel() > 0 then
			setDeployed(self, false)
		else
			if(owner:KeyPressed(IN_JUMP) && owner:GetVelocity().z < 0) then
				setDeployed(self, true)
			end
		end

		--flight code
		if deployed then
			local trueVelocity = owner:GetVelocity()

			--preserve velocity so we don't suddenly stop when exiting noclip
			if owner:GetMoveType() == MOVETYPE_NOCLIP then
				flyVelocity = owner:GetVelocity()
				return
			end

			--bonk!  we hit something
			local collisionSpeed = (trueVelocity - flyVelocity):Length()
			if collisionSpeed > minCrashDamageSpeed  and not justLaunched then
				self:EmitSound(hitSounds[math.random(6)])
				owner:TakeDamage((collisionSpeed -minCrashDamageSpeed) / 10, owner, owner)
				setDeployed(self, false)
			end

			justLaunched = false
			
			--touching walls/crashing affects velocity
			flyVelocity:Add(trueVelocity - flyVelocity)

			--boost
			if owner:KeyDown(IN_ATTACK) then
				--flyVelocity:Add(owner:EyeAngles():Forward() * boostPower)
				if not shaking then
					util.ScreenShake(owner:GetPos(), 5, 5, 10, 10000)
					shaking = true
				end
			else
				shaking = false

				--brake
				if owner:KeyDown(IN_ATTACK2) then
					local resistance = Vector()
					resistance:Set(flyVelocity)
					resistance.z = 0
					resistance:Normalize()
					flyVelocity:Add(resistance * -brakePower)

					if not shaking then
						util.ScreenShake(owner:GetPos(), 5, 5, 10, 10000)
						shaking = true
					end
				end
			end

			--gravity
			flyVelocity.z = flyVelocity.z - 10

			if flyVelocity:Length() > fovIncreaseSpeed then
				if not increasedFOV then
					owner:SetFOV(110, 1)
					increasedFOV = true
				end
			else
				if increasedFOV then
					owner:SetFOV(0, 1)	--0 returns to user's default setting
					increasedFOV = false
				end
			end

			--steering
			--find how fast we are moving relative to out right, and push back accordingly so we go straight again
			local steerForce = owner:GetRight()
			steerForce:Mul(flyVelocity:Dot(owner:GetRight()) * -1)
			steerForce:Normalize()
			steerForce:Mul(flyVelocity:Length() / 20)	--steering power increases as we go faster

			local preSteerSpeed = flyVelocity:Length()
			flyVelocity:Add(steerForce)
			--this steering method slows us down proportionally to how hard we steer
			--so we have to manually preserve our speed
			flyVelocity:Normalize()
			flyVelocity:Mul(preSteerSpeed)

			--lift
			--works pretty much the same as steering, but for up and down instead of left and right
			local preLiftSpeed = flyVelocity:Length()
			local liftForce = owner:GetUp()
			liftForce:Mul(flyVelocity:Dot(owner:GetUp()) * -1)
			liftForce:Normalize()
			liftForce:Mul(flyVelocity:Length() / 20)
			flyVelocity:Add(liftForce)
			flyVelocity:Normalize()
			flyVelocity:Mul(preLiftSpeed)

			--air resistance
			local resistance = Vector()
			resistance:Set(flyVelocity)
			resistance.z = 0
			flyVelocity:Add(resistance * -0.001)

			--apply velocity
			owner:SetVelocity(flyVelocity - trueVelocity)	--when used on players, SetVelocity() actually ADDs the param vector for some reason
		end
	end
end
